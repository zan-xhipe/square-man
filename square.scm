;;; square --------------------------------------------
(define-record square position velocity size colour)

(define x car)
(define y cadr)

;;;; returns a position shifted by dx and dy
(define (alter-position position dx dy)
  (list (+ (x position) dx)
	(+ (y position) dy)))

;;; positions of a squares corners
(define (top-left square)
  (square-position square))
(define (top-right square)
  (alter-position (square-position square) (square-size square) 0))
(define (bottom-left square)
  (alter-position (square-position square) 0 (square-size square)))
(define (bottom-right square)
  (alter-position (square-position square)
		  (square-size square) (square-size square)))

;;; returns a squares edge
(define (left-edge square)
  (x (square-position square)))
(define (right-edge square)
  (+ (left-edge square) (square-size square)))
(define (top-edge square)
  (y (square-position square)))
(define (bottom-edge square)
  (+ (top-edge square) (square-size square)))

(define (draw-square square)
  (filled-rectangle
   (x (square-position square))
   (y (square-position square))
   (square-size square)
   (square-size square)
   (square-colour square)))

(define (move-square! square velocity)
  (square-position-set! square
			(alter-position (square-position square)
					(x velocity)
					(y velocity))))

;;; move the square in the opposite direction to its velocity
(define (move-square-back! square)
  (define (opposite-value n)
    (* n -1))
  (define (opposite-direction)
    (list (opposite-value (x (square-velocity square)))
	  (opposite-value (y (square-velocity square)))))
  (move-square! square (opposite-direction)))

;;; just checks wether two squares overlap
(define (collides? a b)
  (define (make-overlap edge comparison)
    (lambda (a b)
      (if (comparison (edge a) (edge b))
	  a b)))
  ;; functions that check if an edge overlaps
  (define top-overlap? (make-overlap top-edge <))
  (define bottom-overlap? (make-overlap bottom-edge >))
  (define left-overlap? (make-overlap left-edge <))
  (define right-overlap? (make-overlap right-edge >))
  
  (define (horizontal-overlap?)
    (< (top-edge (bottom-overlap? a b)) (bottom-edge (top-overlap? a b))))
  (define (vertical-overlap?)
    (> (right-edge (left-overlap? a b)) (left-edge (right-overlap? a b))))
  (and (horizontal-overlap?) (vertical-overlap?)))

