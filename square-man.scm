
;; a small pacman like game

(use doodle matchable srfi-1 srfi-25)

(use log5scm trace)
(define-category trace)
(start-sender test (port-sender (current-error-port))
	      (output <message) (category trace))


;;; globals -------------------------------------------
(define *player-colour* '(1 1 0 1))
(define *background-colour* '(0 0 1 1))
(define *enemy-colour* '(1 0 0 1))
(define *wall-colour* '(0 0 0 1))
(define *food-colour* '(0 1 0 1))

(define *square-size* 32)
(define *tile-size* 32)
(define *height* 480)
(define *width* 672)

(new-doodle height: *height* width: *width* background: *background-colour*)



;;; square --------------------------------------------
;;; name will provide type information (player, enemy, wall)
;;; position is a list of length 2
;;; velocity is a list of length 2
;;; size is the height and width of the square in pixels
;;; colour is a list of length 4, RGBA
(define-record square name position velocity size colour)

(define (x pos)
  (car pos))
(define (y pos)
  (cadr pos))

;;;; returns a position shifted by dx and dy
(define (alter-position position dx dy)
  (list (+ (x position) dx)
	(+ (y position) dy)))

;;; positions of a squares corners
(define (top-left square)
  (square-position square))
(define (top-right square)
  (alter-position (square-position square) (square-size square) 0))
(define (bottom-left square)
  (alter-position (square-position square) 0 (square-size square)))
(define (bottom-right square)
  (alter-position (square-position square)
		  (square-size square) (square-size square)))

;;; returns a squares edge
(define (left-edge square)
  (x (square-position square)))
(define (right-edge square)
  (+ (left-edge square) (square-size square)))
(define (top-edge square)
  (y (square-position square)))
(define (bottom-edge square)
  (+ (top-edge square) (square-size square)))

(define (draw-square square)
  (filled-rectangle
   (x (square-position square))
   (y (square-position square))
   (square-size square)
   (square-size square)
   (square-colour square)))

(define (move-square! square velocity)
  (square-position-set! square
			(alter-position (square-position square)
					(x velocity)
					(y velocity))))


;;; just checks wether two squares overlap
(define (collides? a b)
  (define (make-overlap edge comparison)
    (lambda (a b)
      (if (comparison (edge a) (edge b))
	  a b)))
  ;; functions that check if an edge overlaps
  (define top-overlap? (make-overlap top-edge <))
  (define bottom-overlap? (make-overlap bottom-edge >))
  (define left-overlap? (make-overlap left-edge <))
  (define right-overlap? (make-overlap right-edge >))
  
  (define (horizontal-overlap?)
    (< (top-edge (bottom-overlap? a b)) (bottom-edge (top-overlap? a b))))
  (define (vertical-overlap?)
    (> (right-edge (left-overlap? a b)) (left-edge (right-overlap? a b))))
  (and (horizontal-overlap?) (vertical-overlap?)))


;;; level ---------------------------------------------
;;; width and height are measured in pixels
;;; tile-size is the size of grid tiles
;;; tiles holds an array that is the collision detection grid
(define-record level width height tile-size tiles)

;;; returns a function that provides the maximum tile number
;;; in a direction.
(define (make-max-x/y size)
  (lambda ()
    (inexact->exact (floor (/ size *tile-size*)))))

(define x-max (make-max-x/y doodle-width))
(define y-max (make-max-x/y doodle-height))

(define *level* (make-level
		 doodle-width doodle-height *tile-size*
		 (make-array (shape 0 (y-max)
				    0 (x-max))
			     (list))))

;;; returns a function that accepts a number
;;; and returns a number between the provided in and max.
;;; if the provided number is in this range then it is unchanged
(define (make-clamp min max)
  (lambda (n)
    (cond ((> n max) max)
	  ((< n min) min)
	  (else n))))

;;; clamps for the grid
(define x-clamp (make-clamp 0 (- (x-max) 1)))
(define y-clamp (make-clamp 0 (- (y-max) 1)))

;;; positions outside the grid are put into the closest edge of the grid
(define (tile-ref level position)
  (array-ref (level-tiles level) (y-clamp (y position)) (x-clamp (x position))))

(define (tile-set! level position value)
  (array-set! (level-tiles level) (y-clamp (y position)) (x-clamp (x position)) value))

;;; translate a position to a tile.
(define (position->tile position level)
  (list (inexact->exact (floor (/ (x position) (level-tile-size level))))
	(inexact->exact (floor (/ (y position) (level-tile-size level)))))) 

;;; returns all the tiles that a square occupies.
;;; used to figure out where on the grid the square is.
(define (square->tiles square level)
  (delete-duplicates
   (map (lambda (corner-function)
	  (position->tile (corner-function square) level))
	(list top-left top-right bottom-left bottom-right))))

;;; inserts a square into the levels grid
(define (insert-square! square level)
  (for-each (lambda (pos)
	      (tile-set! level pos
			 (cons square (tile-ref level pos))))
	    (square->tiles square level)))

;;; reoves a square from the levels grid
(define (remove-square! square level)
  (for-each (lambda (pos)
	      (tile-set! level pos
			 (remove (lambda (sq)
				   (eqv? sq square))
				 (tile-ref level pos))))
	    (square->tiles square level)))

;;; a broad collision check. it returns a lit of objects the the square could
;;; possibly collide with
(define (find-neighbours square level)
  (apply append (map (lambda (pos)
		       (apply append (list (remove (lambda (sq)
						(eqv? sq square))
					      (tile-ref level pos)))))
  		     (square->tiles square level))))


(define (move-in-level! square velocity level)
  (remove-square! square level)
  (move-square! square velocity)
  (insert-square! square level))

(define (opposite-direction velocity)
  (list (* (x velocity) -1)
	(* (y velocity) -1)))

;;; accepts a broad function and a narrow function
;;; the braod function takes the objects to be checked]
;;; and some form of the world it can collide with
;;; and returns a list of all things the object could collide with.
;;; the narrow function takes the objects and the list of objects
;;; it could collide with and checks wether it actually collides
;;; with any objects on that list. It returns a list of objects that
;;; it actually collides with.
;;; make-collision-check returns a function that accepts a objects and a world
;;; and returns a list of objects that the object collides with.
(define (make-collision-check broad narrow)
  (lambda (square level)
    (remove not (map (lambda (other)
	    (narrow square other))
	  (broad square level)))))

(define check-collision (make-collision-check find-neighbours collides?))

;;; currently only moves the square
;;; and checks if it collides with anything
(define (update-square! square level)
  (move-in-level! square (square-velocity square) level)
  (let ((collisions (check-collision square level)))
    (if (not (null? collisions))
	(move-in-level! square
			(opposite-direction (square-velocity square)) level))
    collisions))

(define (make-find-surrounds x-fun y-fun)
  (lambda (tile)
    (array-ref *level*
	       (x-fun (x (tile-position tile)))
	       (y-fun (y (tile-position tile))))))

(define above (make-find-surrounds (lambda (x) x)
				   (lambda (y) (- y 1))))
(define below (make-find-surrounds (lambda (x) x)
				   (lambda (y) (+ y 1))))
(define left (make-find-surrounds (lambda (x) (- x 1))
				  (lambda (y) y)))
(define right (make-find-surrounds (lambda (x) (+ x 1))
				   (lambda (y) y)))

(define (surroundings tile)
  (list (above tile)
	(below tile)
	(left tile)
	(right tile)))


;;; enemies -------------------------------------------
;;; at the moment an enemy is just a square that the player can't control
(define *enemies* (list))

(define (random-velocity)
  (define (-1to1)
    (case (random 3)
      ((0) 0)
      ((1) 1)
      ((2) -1)))
  (let ((velocity (list (-1to1) (-1to1))))
    (if (not (= 1 (+ (abs (x velocity)) (abs (y velocity)))))
	(random-velocity)
	velocity)))

(define (add-enemy pos)
  (let ((square (make-square
		 "enemy" pos (random-velocity) *square-size* *enemy-colour*)))
    (set! *enemies* (cons square *enemies*))
    (insert-square! square *level*)))



;;; moves a enemy in random directions
;;; will later contain more significant AI
(define (update-enemy! square level)
  (let ((collisions (update-square! square level)))
    (if (not (null? collisions))
	(square-velocity-set! square (random-velocity))
	)
   ))

(define (update-enemies!)
  (for-each (lambda (enemy)
	      (update-enemy! enemy *level*)) *enemies*))

(define (draw-enemies)
  (for-each draw-square *enemies*))

(define (add-random-enemy)
  (add-enemy (list (random (- doodle-width 32)) (random (- doodle-height 32)))))

;;; walls -----------------------------------------------
(define *walls* (list))

(define (add-wall pos)
  (let ((square (make-square "wall" pos '(0 0) *square-size* *wall-colour*)))
    (set! *walls* (cons square *walls*))
    (insert-square! square *level*)))

(define (draw-walls)
  (for-each draw-square *walls*))

;;; player -----------------------------------------------

(define *player* (make-square "player" '(32 32) '(0 0) *square-size* *player-colour*))

;;; other ------------------------------------------------

(define (event-handler events)
  (for-each
   (lambda (e)
     (match e
	    (('key 'pressed #\q)
	     (quit #t))
	    (('key 'pressed 'down)
	     (square-velocity-set! *player* '(0 1)))
	    (('key 'pressed 'up)
	     (square-velocity-set! *player* '(0 -1)))
	    (('key 'pressed 'left)
	     (square-velocity-set! *player* '(-1 0)))
	    (('key 'pressed 'right)
	     (square-velocity-set! *player* '(1 0)))
	    (('key 'pressed #\e)
	     (add-random-enemy))
	    (else (void))))
   events))

(add-enemy '(100 100))

(define (print a)
  (display a)
  (newline))


(define (load-map file)
  (let ((size #f) (symbol->tile #f) (level #f))
    (with-input-from-file file
      (lambda ()
	(set! size (read))
	(set! symbol->tile (eval (read)))
	(set! level (read))))
    (define (make-row y row)
      (let ((x 0))
	(for-each (lambda (sym)
		    (symbol->tile sym (* x size) (* y size))
		    (set! x (+ x 1)))
		  row)))
    (define (load-all all)
      (let ((y 0))
	(for-each (lambda (row)
		    (make-row y row)
		    (set! y (+ y 1)))
		  all)))
    (load-all level)))

(load-map "test.level")

  (world-changes
   (lambda (events dt escape-continuation)
     (event-handler events)
     (clear-screen)
     (update-square! *player* *level*)
     (draw-square *player*)
     (update-enemies!)
     (text 50 50 (format "#enemies: ~A" (length *enemies*)))
     (draw-enemies)
     (draw-walls)))
(world-ends (lambda ()
	     (exit 0)))

(run-event-loop)

